//
//  WeatherApp.swift
//  Weather
//
//  Created by Kanneganti, Dharma Teja - RTS on 5/23/23.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
