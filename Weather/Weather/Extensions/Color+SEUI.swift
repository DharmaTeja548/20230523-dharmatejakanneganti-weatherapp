//
//  Color+SEUI.swift
//  Weather
//
//  Created by Kanneganti, Dharma Teja - RTS on 5/23/23.
//

import SwiftUI

extension Color {
    static var brand01: Color {
        return Color(UIColor.brand01)
    }
}
